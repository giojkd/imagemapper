
<!DOCTYPE html>
<html>
<head>
	<title>Imagemapper</title>
	<link href="/style.css" type="text/css" rel="stylesheet" />
	<meta charset="UTF-8" />
</head>
<body>
<noscript>
	<div id="noscript">
		Please, enable javascript in your browser
	</div>?
</noscript>

<div id="wrapper">
	<header id="header">

		<nav id="nav" class="clearfix">
			<ul>
				<li style="display: none" id="save"><a href="#">save</a></li>
				<li style="display: none" id="load"><a href="#">load</a></li>
                <li  style="display: none"  id="from_html"><a href="#"></a></li>
                <li><a href="/">Esci</a></li>
				<li id="rect"><a href="#">Rettangolo</a></li>
				<li id="circle"><a href="#">Cerchio</a></li>
				<li id="polygon"><a href="#">Poligono</a></li>
				<li id="edit"><a href="#">Modifica</a></li>
				<li style="display: none" id="to_html"><a href="#">Html</a></li>
				<li style="display: none" id="preview"><a href="#">Anteprima</a></li>
				<li id="clear"><a href="#">Cancella</a></li>
				<li id="new_image"><a href="#">Nuova immagine</a></li>
                <li id="show_help"><a href="#">Aiuto</a></li>
                <li>
                    @if(isset($cmap))
                    <form id="save_map_form" style="display: inline" action="{{ Route('mapUpdate',['id' => $cmap->id]) }}" method="post">
                        @csrf
                        <input type="text" id="map_name" placeholder="Nome della mappa" name="name">
                        <input type="hidden" id="imageUpload" name="imageUpload">
                        <input type="hidden" id="imageName" name="imageName">
                        <input type="hidden" id="map" name="map">
                    </form>
                    @else

                         <form id="save_map_form" style="display: inline" action="{{ Route('mapStore') }}" method="post">
                            @csrf
                            <input type="text" id="map_name" placeholder="Nome della mappa" name="name">
                            <input type="hidden" id="imageUpload" name="imageUpload">
                            <input type="hidden" id="imageName" name="imageName">
                            <input type="hidden" id="map" name="map">
                        </form>

                    @endif
                    <a href="#" id="save_map">Salva</a>
                </li>





			</ul>
		</nav>
		<div id="coords"></div>
		<div id="debug"></div>
	</header>
	<div id="image_wrapper">
		<div id="image">
			<img src="" alt="#" id="img" />
			<svg xmlns="http://www.w3.org/2000/svg" version="1.2" baseProfile="tiny" id="svg"></svg>
		</div>
	</div>
</div>

<!-- For html image map code -->
<div id="code">
	<span class="close_button" title="close"></span>
	<div id="code_content"></div>
</div>

<!-- Edit details block -->
<form id="edit_details">
	<h5>Attributi</h5>
	<span class="close_button" title="close"></span>
	<p>
		<label for="href_attr">href</label>
		<input type="text" id="href_attr" />
	</p>
	<p>
		<label for="alt_attr">alt</label>
		<input type="text" id="alt_attr" />
	</p>
	<p>
		<label for="title_attr">title</label>
		<input type="text" id="title_attr" />
    </p>
    <p>
        <label for="target_attr">target</label>
        <select name="" id="target_attr">
            <option value="_self">_self</option>
            <option value="_blank">_blank</option>
            <option value="_parent">_parent</option>
            <option value="_top">_top</option>
        </select>

    </p>
     <p>
		<label for="onclick_attr">onclick</label>
		<input type="text" id="onclick_attr" />
    </p>
	<button id="save_details">Salva</button>
</form>

<!-- From html block -->
<div id="from_html_wrapper">
	<form id="from_html_form">
		<h5>Loading areas</h5>
		<span class="close_button" title="close"></span>
		<p>
			<label for="code_input">Enter your html code:</label>
			<textarea id="code_input"></textarea>
		</p>
		<button id="load_code_button">Load</button>
	</form>
</div>

<!-- Get image form -->
<div id="get_image_wrapper">
	<div id="get_image">

		<div id="loading">Loading</div>
		<div id="file_reader_support">
			<label>Trascina qui un'immagine</label>
			<div id="dropzone">
				<span class="clear_button" title="clear">x</span>
				<img src="" alt="preview" id="sm_img" />
			</div>
			<b style="display: none">or</b>
		</div>
		<label style="display: none" for="url">type a url</label>
		<span style="display: none" id="url_wrapper">
			<span class="clear_button" title="clear">x</span>
			<input type="text" id="url" />
		</span>
		<button id="button">Avanti</button>
	</div>
</div>

<!-- Help block -->
<div id="overlay"></div>
<div id="help">
	<span class="close_button" title="close"></span>
	<div class="txt">
		<!--<section>
			<h2>Main</h2>
			<p><span class="key">F5</span> &mdash; reload the page and display the form for loading image again</p>
			<p><span class="key">S</span> &mdash; save map params in localStorage</p>
        </section>-->

		<section>
			<h2>Modalità disegno (rettangolo / cerchio / poligono)</h2>
			<p><span class="key">ENTER</span> &mdash; termina il disegno del poligono (o fai click sul primo punto)</p>
			<p><span class="key">ESC</span> &mdash; cancella il disegno di una nuova area</p>
			<p><span class="key">SHIFT</span> &mdash; disegna un quadrato nel caso di un rettangolo e angolo retto nel caso di un poligono</p>
		</section>
		<section>
		<h2>Modalità modifica</h2>
			<p><span class="key">DELETE</span> &mdash; elimina un'area selezionata</p>
			<p><span class="key">ESC</span> &mdash; annulla la modifica di un'area selezionata</p>
			<p><span class="key">SHIFT</span> &mdash; modifica e salva le proporzioni del rettangolo</p>
			<p><span class="key">I</span> &mdash; modifica gli attributi di un'area selezionata (o fai doppio click)</p>
			<p><span class="key">CTRL</span> + <span class="key">C</span> &mdash; crea una copia dell'area selezionata</p>
			<p><span class="key">&uarr;</span> &mdash; sposta in su un'area selezionata</p>
			<p><span class="key">&darr;</span> &mdash; sposta in giù un'area selezionata</p>
			<p><span class="key">&larr;</span> &mdash; sposta a sinistra un'area selezionata</p>
			<p><span class="key">&rarr;</span> &mdash; sposta a destra un'area selezionata</p>
		</section>
	</div>
</div>

<script type="text/javascript" src="/scripts.js?v={{ rand(0,100000) }}"></script>

<script>

    var appUrl = '{{ env('APP_URL') }}';



    var mapId = 0;
    document.addEventListener("DOMContentLoaded",function(e){
        document.getElementById("save_map").addEventListener("click",function(e){
            e.preventDefault();
            document.getElementById("imageUpload").value = document.getElementById("img").getAttribute('src');
            document.getElementById("to_html").click();
            document.getElementById("save_map_form").submit();
        })
    });
</script>

@if(isset($cmap))
    <script>
        mapId = {{ $cmap->id }};
        document.addEventListener("DOMContentLoaded", function(){
            app.loadImage("data:image/jpg;base64,{{ $image }}").setFilename('{{ $filename }}');
            document.getElementById("code_input").value = '{!! addslashes(str_replace("\r\n","",$cmap->map)) !!}';
            document.getElementById("map_name").value = '{{ $cmap->name }}';
            document.getElementById("load_code_button").click();
        }, false);
    </script>
@endif

</body>
</html>
