<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Map;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        #   return view('home');
        $data['maps'] = Map::orderBy('id', 'DESC')->get();
        return view('home', $data);
    }

    public function test(){
        return view('test');
    }
}
