<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::post('/map/store', 'MapController@store')->name('mapStore');
Route::post('/map/delete', 'MapController@delete')->name('mapDelete');
Route::get('/map/edit/{id}', 'MapController@edit')->name('mapEdit');
Route::post('/map/update/{id}', 'MapController@update')->name('mapUpdate');
Route::get('/map/new', 'MapController@new')->name('newMap');
Route::post('/map/change-img', 'MapController@changeImg')->name('changeImg');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test','HomeController@test')->name('test');
