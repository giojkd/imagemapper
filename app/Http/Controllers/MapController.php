<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Str;
use Storage;
use App\Map;


class MapController extends Controller
{
    //
    public function store(Request $request)
    {

        #dd($request->all());

        $image_64 = $request->imageUpload; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        Storage::disk('public')->put($request->imageName, base64_decode($image));

        $map = Map::create([
            'areas' => $request->areas,
            'name' => $request->name,
            'image' => 'storage/' . $request->imageName,
            'map' => $request->map
        ]);

        return redirect()->route('mapEdit', ['id' => $map->id]);
    }

    public function new(){
        $data = [];
        return view('map_editor', $data);
    }

    public function update(Request $request, $id){
        $map = Map::findOrFail($id);
        #dd($request->all());
        $image_64 = $request->imageUpload; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        Storage::disk('public')->put($request->imageName, base64_decode($image));

        $map->fill([
            #'areas' => $request->areas,
            'name' => $request->name,
            'image' => 'storage/' . $request->imageName,
            'map' => $request->map
        ]);

        $map->save();

        return redirect()->route('mapEdit',['id' => $map->id]);
    }

    public function edit($id)
    {

        $map = Map::findOrFail($id);
        $data = [];

        $image = Storage::disk('public')->get(Str::replaceFirst('storage/', '', $map->image));

        $map->areas = json_decode($map->areas); #??????

        $data['image'] = base64_encode($image);
        $data['maps'] = Map::orderBy('id', 'asc')->get();
        $data['cmap'] = $map;
        $data['filename'] = basename($map->image);

        #dd($data);

        return view('map_editor', $data);

    }

    public function delete(Request $request)
    {
        $map = Map::findOrFail($request->id);
        $map->delete();
        return back();
    }


    public function changeImg(Request $req){
        $map = Map::findOrFail($req->id);

        $req->validate([
            'file' => 'required|mimes:jpg,jpeg,png,JPG,JPEG'
        ]);

        if ($req->file()) {
            $fileName = time() . '_'. $req->id.'_' . $req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('', $fileName, 'public');
            $map->image = 'storage/'.$filePath;
            $map->save();
            return back();
        }
    }

}
