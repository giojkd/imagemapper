@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container">
         <div class="row">
            <div class="col-md-12">
                <h2>Mappe <a href="{{ Route('newMap') }}" class="btn btn-primary">+</a> </h2>
                @if(!is_null($maps) && $maps->count() > 0)
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Contenuto</th>
                            <th>Azioni</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($maps as $map)
                            <tr>
                                <td>
                                    <img src="/{{ $map->image }}" alt="" style="height: 50px">
                                </td>
                                <td>{{ $map->id }}</td>
                                <td>{{ $map->name }}</td>
                                <td>
                                    <textarea id="code-{{ $map->id }}" class="form-control">{{ str_replace('***MapId***',$map->id,str_replace('***MapImgSrc***',env('APP_URL').'/'.$map->image,$map->map)) }}
                                        <script>window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"><\/script>')</script>
                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.min.js"></script>
                                        <script>
                                            $(function(){
                                                $('.usemap_{{ $map->id }}').each(
                                                    function(){
                                                        var src = $(this).attr('src');
                                                        src = src+='?v='+Math.floor(Math.random()*100000)+1;
                                                        $(this).attr('src',src);
                                                    });
                                                    var timerImageMapper = setInterval(
                                                        function(){
                                                            if($('.usemap_{{ $map->id }}').width() > 0){
                                                                $('.usemap_{{ $map->id }}').rwdImageMaps();
                                                                clearInterval(timerImageMapper);
                                                            }},
                                                    200);
                                            })
                                        </script>
                                    </textarea>
                                    <a href="javascript:" onclick="copyCode({{ $map->id }})">copy</a>
                                </td>
                                <td>
                                   <a href="{{ Route('mapEdit',['id' => $map->id]) }}" class="btn btn-primary">Modifica</a>
                                    <form action="{{ Route('mapDelete') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $map->id }}">
                                        <a onclick="if(confirm('Sei sicuro di voler eliminare la mappa {{ $map->id }}?')){ $(this).closest('form').submit(); }" class="btn btn-danger">Cancella</a>
                                    </form>
                                    <form action="{{ route('changeImg') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $map->id }}">
                                        <div class="form-group">
                                            <input type="file" name="file" >
                                        </div>
                                        <button class="btn btn-primary">Cambia img</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <div class="alert alert-secondary">
                        Non hai creato nessuna mappa ancora
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
<script>
    function copyCode(id){

        /* Get the text field */
  var copyText = document.getElementById("code-"+id);

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Mappa "+id+" copiata negli appunti");
    }
</script>
@endsection
